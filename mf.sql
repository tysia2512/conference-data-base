--Martyna Siejba

DROP TABLE IF EXISTS participant CASCADE;
DROP TABLE IF EXISTS event CASCADE;
DROP TABLE IF EXISTS talk CASCADE;
DROP TABLE IF EXISTS talk_proposal CASCADE;
DROP TABLE IF EXISTS rejected_talk CASCADE;
DROP TABLE IF EXISTS room CASCADE;
DROP TABLE IF EXISTS talk_in_room CASCADE;
DROP TABLE IF EXISTS participant_registered_event CASCADE;
DROP TABLE IF EXISTS participant_attends_talk CASCADE;
DROP TABLE IF EXISTS participant_rated_talk CASCADE;
DROP TABLE IF EXISTS participant_friend_participant CASCADE;

CREATE TABLE participant(
    login VARCHAR(255) PRIMARY KEY,
    hash VARCHAR(255) NOT NULL,
    organizer BIT NOT NULL
);

CREATE TABLE event(
    name VARCHAR(255) PRIMARY KEY,
    begin_time TIMESTAMP NOT NULL,
    end_time TIMESTAMP NOT NULL,
    CONSTRAINT correct_time CHECK (begin_time < end_time)
);

CREATE TABLE talk(
    id VARCHAR(255) PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    eventname VARCHAR(255) NOT NULL,
    participantlogin VARCHAR(255) NOT NULL,
    begin_time TIMESTAMP NOT NULL,
    room VARCHAR(255) NOT NULL,
    FOREIGN KEY (eventname) REFERENCES event(name),
    FOREIGN KEY (participantlogin) REFERENCES participant(login) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE talk_proposal(
    id VARCHAR(255) PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    participantlogin VARCHAR(255) NOT NULL,
    begin_time TIMESTAMP NOT NULL,
    FOREIGN KEY (participantlogin) REFERENCES participant(login) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE rejected_talk(
    id VARCHAR(255) PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    participantlogin VARCHAR(255) NOT NULL,
    begin_time TIMESTAMP NOT NULL,
    FOREIGN KEY (participantlogin) REFERENCES participant(login) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE OR REPLACE FUNCTION talks_during_event(talk)
RETURNS BOOLEAN
AS
$X$
    DECLARE collisions BIGINT;
    BEGIN
        SELECT COUNT (*) FROM event INTO collisions
        WHERE $1.eventname = event.name 
            AND (date($1.begin_time) < event.begin_time
                OR date($1.begin_time) > event.end_time);
        RETURN (collisions = 0);
    END;
$X$
LANGUAGE plpgsql;

ALTER TABLE talk ADD CONSTRAINT talks_in_events CHECK (talks_during_event(talk));

CREATE TABLE participant_registered_event(
    participantlogin VARCHAR(255) NOT NULL,
    eventname VARCHAR(255) NOT NULL,
    FOREIGN KEY (participantlogin) REFERENCES participant(login) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (eventname) REFERENCES event(name) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (participantlogin, eventname)
);

CREATE TABLE participant_attends_talk(
    participantlogin VARCHAR(255) NOT NULL,
    talkid VARCHAR(255) NOT NULL,
    FOREIGN KEY (participantlogin) REFERENCES participant(login) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (talkid) REFERENCES talk(id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (participantlogin, talkid)
);

CREATE OR REPLACE FUNCTION participant_attends_talk_registered(
    VARCHAR(255), VARCHAR(255)
)
RETURNS BIGINT
AS
$X$
    DECLARE res BIGINT;
    BEGIN
        SELECT COUNT (*) INTO res FROM participant_registered_event
        WHERE participantlogin = $1 
        AND eventname = $2;
        RETURN res;
    END;          
$X$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION unregistered(participant_attends_talk)
RETURNS BIGINT
AS
$X$
    DECLARE res BIGINT;
    BEGIN
        SELECT COUNT (*) INTO res FROM 
        talk WHERE $1.talkid = talk.id
        AND participant_attends_talk_registered(
                $1.participantlogin, eventname
                ) = 0;
        RETURN res;
    END;  
$X$
LANGUAGE plpgsql;

CREATE TABLE participant_rated_talk(
    participantlogin VARCHAR(255) NOT NULL,
    talkid VARCHAR(255) NOT NULL,
    rating INT NOT NULL,
    FOREIGN KEY (participantlogin) REFERENCES participant(login), --ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (talkid) REFERENCES talk(id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (participantlogin, talkid)
);

CREATE TABLE participant_friend_participant(
    participantlogin1 VARCHAR(255) NOT NULL,
    participantlogin2 VARCHAR(255) NOT NULL,
    mutual BOOLEAN NOT NULL,
    FOREIGN KEY (participantlogin1) REFERENCES participant(login) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (participantlogin2) REFERENCES participant(login) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (participantlogin1, participantlogin2)
);

ALTER TABLE participant_friend_participant ADD CONSTRAINT self_invitation
CHECK (participantlogin1 != participantlogin2);

DROP FUNCTION IF EXISTS invite(VARCHAR(255), VARCHAR(255));
CREATE OR REPLACE FUNCTION invited(VARCHAR(255), VARCHAR(255))
RETURNS BOOLEAN
AS
$X$
    DECLARE invitation INT;
    BEGIN
        SELECT COUNT (*) FROM participant_friend_participant INTO invitation
        WHERE (participantlogin1 = $1 
            AND participantlogin2 = $2)
            OR (participantlogin1 = $2 
            AND participantlogin2 = $1
            AND mutual);
        RETURN (invitation > 0);
    END;
$X$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION friends(VARCHAR(255), VARCHAR(255))
RETURNS BOOLEAN
AS
$X$
    DECLARE invitation INT;
    BEGIN
        SELECT COUNT (*) FROM participant_friend_participant INTO invitation
        WHERE (participantlogin1 = $1 
            AND participantlogin2 = $2
            AND mutual)
            OR (participantlogin1 = $2 
            AND participantlogin2 = $1
            AND mutual);
        RETURN (invitation > 0);
    END;
$X$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION invite(VARCHAR(255), VARCHAR(255))
RETURNS BOOLEAN
AS
$X$
BEGIN
    IF friends($1, $2)
    THEN RETURN FALSE;
    ELSEIF (invited($2, $1))
        THEN UPDATE participant_friend_participant
        SET mutual = TRUE
        WHERE participantlogin1 = $2 AND participantlogin2 = $1;
    ELSE INSERT INTO participant_friend_participant(participantlogin1, participantlogin2, mutual)
    VALUES ($1, $2, FALSE);
    END IF;
    RETURN TRUE;
END;
$X$
LANGUAGE plpgsql;

DROP ROLE IF EXISTS superuser;
CREATE ROLE superuser WITH SUPERUSER;
