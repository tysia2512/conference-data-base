import functions
import json
import fileinput
import sys
import string

def returned_value_to_json(returned, function_name):
    if (returned == 'OK' 
    or returned == 'ERROR' 
    or returned == 'NOT IMPLEMENTED'):
        return json.dumps({'status':returned})
    return json.dumps({'status':'OK', 'data': returned})


indata = sys.stdin.read()
for line in string.split(indata, '\n'):
    parsed_json = json.loads(line)
    function_name = parsed_json.keys()[0]
    args = parsed_json[function_name]
    for key in args:
        try:
            args[key] = args[key].encode('utf-8') 
        except:
            pass
            # int value (rating, limit)
    if function_name == "open":
        function_name = "open1"
    function = getattr(functions, function_name)
    print returned_value_to_json(function(**args), function_name)
