# 1. zapobiec sql injection
# 2. o co chodzi z logowaniem w open
# 3. constraint na nachodzace na siebie w tej samej sali referaty
# 4. constraint na talk w czasie eventu
# 5. naprawic timestampy eventow - na godziny

import psycopg2
import string
import hashlib, uuid
import sql_calls as sc
import bcrypt
import json
from collections import OrderedDict

class NotOrganizer(Exception):
    def __init__(self):
        pass

class NotParticipant(Exception):
    def __init__(self):
        pass

cur = 0

def hash_password(password):
    hashed = bcrypt.hashpw(password, bcrypt.gensalt())
    return hashed

def check_password(password, hashed_password):
    return bcrypt.checkpw(password, hashed_password)

def log_in(login, password):
    participant = sc.find_participant(cur, login)
    if participant is None:
        return None
    else:
        _, hash, organizer = participant
        if check_password(password, hash):
            return organizer
        else:
            return None

def for_organizer(login, password):
    if log_in(login, password) == '1':
        return None
    else:
        raise NotOrganizer

def for_participant(login, password):
    if log_in(login, password) == '0' or log_in(login, password) == '1':
        return None
    else:
        raise NotParticipant

def open1(baza, login, password):
    try:
        conn = psycopg2.connect("dbname='{0}' user='{1}' password='{2}'".format(baza, login, password))
        global cur 
        cur = conn.cursor()
        cur.execute(open("mf.sql", "r").read())
        return 'OK'
    except:
        return 'ERROR'

def organizer(secret, newlogin, newpassword):
    if secret == 'd8578edf8458ce06fbc5bb76a58c5ca4':
        hash = hash_password(newpassword)
        try:
            sc.add_participant(cur, newlogin, hash, '1')
            return 'OK'
        except:
            return 'ERROR'
    else:
        return 'ERROR'

def event(login, password, eventname, start_timestamp, end_timestamp):
    try:
        for_organizer(login, password)
        sc.add_event(cur, eventname, start_timestamp, end_timestamp)
        return 'OK'
    except:
        return 'ERROR'

def user(login, password, newlogin, newpassword):
    try:
        for_organizer(login, password)
        sc.add_participant(cur, newlogin, hash_password(newpassword), '0')
        return 'OK'
    except:
        return 'ERROR'

def talk(
    login, password, 
    speakerlogin, talk, title, start_timestamp, room, initial_evaluation, eventname
    ):
    try:
        for_organizer(login, password)
        sc.add_talk(
            cur, login,
            speakerlogin, talk, title, start_timestamp, 
            room, initial_evaluation, eventname
            )
        return 'OK'
    except:
        return 'ERROR'

def register_user_for_event(login, password, eventname):
    try:
        for_participant(login, password)
        sc.add_participant_registered_event(cur, login, eventname)
        return 'OK'
    except:
        return 'ERROR'

def attendance(login, password, talk):
    try:
        for_participant(login, password)
        sc.add_participant_attends_talk(cur, login, talk)
        return 'OK'
    except:
        return 'ERROR'

def evaluation(login, password, talk, rating):
    try:
        for_participant(login, password)
        sc.add_rating(cur, talk, login, rating)
        return 'OK'
    except:
        return 'ERROR'

def reject(login, password, talk):
    try:
        for_organizer(login, password)
        sc.reject(cur, talk)
        return 'OK'
    except:
        return 'ERROR'

def proposal(login, password, talk, title, start_timestamp):
    try:
        for_participant(login, password)
        sc.proposal(cur, talk, title, login, start_timestamp)
        return 'OK'
    except:
        raise
        return 'ERROR'

def friends(login1, password, login2):
    try:
        for_participant(login1, password)
        sc.friend_request(cur, login1, login2)
        return 'OK'
    except:
        return 'ERROR'

def user_plan(login, limit):
    try:
        if limit != 0:
            plan = sc.user_plan(cur, login)[:limit]
        else:
            plan = sc.user_plan(cur, login)
        return map(
            lambda t: OrderedDict([('login', t[5]), ('talk', t[1]), 
            ('start_timestamp', str(t[2])),
            ('title', t[3]), ('room', t[4])]),
            plan
        )
    except:
        return 'ERROR'

def day_plan(timestamp):
    try:
        return map(
            lambda t: OrderedDict([('talk', t[0]), ('start_timestamp', str(t[1])),
            ('title', t[2]), ('room', t[3])]),
            sc.day_plan(cur, timestamp)
        )
    except:
        return 'ERROR'

def mean(talk, all):
    if all == 1:
        ratings = sc.all_ratings(cur, talk)
    elif all == 0:
        ratings = sc.attended_only_ratings(cur, talk)
    else:
        raise Exception('InputError: all in mean')
    ratings = map(lambda r: r[2], ratings)
    if ratings == []:
        return -1.0
    else:
        return float(reduce(lambda x, y: x+y, ratings)) / float(len(ratings))

def best_talks(start_timestamp, end_timestamp, limit, all):
    try:
        talks_between = sc.talks_between(cur, start_timestamp, end_timestamp)
        talks_between = map(lambda t: (-mean(t[0], all), ) + t, talks_between)
        talks_between.sort()
        talks_between = map(lambda t: (t[1], t[5], t[2], t[6]), talks_between)
        if limit == 0:
            talks = talks_between
        else:
            talks = talks_between[:limit]
        return map(
            lambda t: OrderedDict([('talk', t[0]), 
            ('start_timestamp', str(t[1])),
            ('title', t[2]), ('room', t[3])]),
            talks
        )
    except:
        return 'ERROR'

def attendance_to_talk(talk):
    attented = sc.attendance_to_talk(cur, talk)
    return len(attented)

def most_popular_talks(start_timestamp, end_timestamp, limit):
    try:
        talks = sc.talks_between(cur, start_timestamp, end_timestamp)
        talks_attended = map(
            lambda t: (-attendance_to_talk(t[0]), ) + t, 
            talks
            )
        talks_attended.sort()
        talks_attended = map(
            lambda t: (t[1], t[5], t[2], t[6]),
            talks_attended 
        )
        if limit == 0:
            talks = talks_attended
        else:
            talks = talks_attended[:limit]
        return map(
            lambda t: OrderedDict([('talk', t[0]),
            ('start_timestamp', str(t[1])), ('title', t[2]), ('room', t[3])]),
            talks
        )
    except:
        return 'ERROR'

def attended_talks(login, password):
    try:
        for_participant(login, password)
        attended = sc.participant_attended_talks(cur, login)
        return map(
            lambda t: OrderedDict([('talk', t[1]),
            ('start_timestamp', str(t[6])), ('title', t[3]), ('room', t[7])]),
            attended
        )
    except:
        return 'ERROR'

def abandoned_talks(login, password, limit):
    try:
        for_organizer(login, password)
        talks = sc.all_talks(cur)
        talks = map(    
            lambda t: (
                -(len(sc.registered_to_event(cur, t[2])) 
                - len(sc.attendance_to_talk(cur, t[0]))),
                ) + t,
            talks
        )
        # <talk> <start_timestamp> <title> <room> <number>
        talks.sort()
        return map(
            lambda t: OrderedDict([
                ('talk', t[1]), ('start_timestamp', str(t[5])),
                ('title', t[2]), ('room', t[6]), ('number', -t[0])

            ]),
            talks
        )
    except:
        return 'ERROR'

def recently_added_talks(limit):
    return 'NOT IMPLEMENTED'

def rejected_talks(login, password):
    format_to_return = lambda t: OrderedDict([
        ('talk', t[0]), ('speakerlogin', t[2]),
        ('start_timestamp', str(t[3])), ('title', t[1])
    ])
    try:
        for_organizer(login, password)
        return map(format_to_return, sc.all_rejected_talks(cur))
    except NotOrganizer:
        for_participant(login, password)
        return map(format_to_return, sc.users_rejected_talks(cur, login))
    except:
        return 'ERROR'        

def proposals(login, password):
    try:
        for_organizer(login, password)
        return map(
            lambda t: OrderedDict([
                ('talk', t[0]), ('speakerlogin', t[2]),
                ('start_timestamp', str(t[3])), ('title', t[1])
            ]),
            sc.proposals(cur)
        )
    except:
        return 'ERROR'

def friends_talks(login, password, start_timestamp, end_timestamp, limit):
    try:
        for_participant(login, password)
        talks = sc.friends_talks(cur, login, start_timestamp, end_timestamp)
        if limit != 0:
            talks = talks[:limit]
        return map(
            lambda t: OrderedDict([
                ('talk', t[0]), ('speakerlogin', t[3]),
                ('start_timestamp', str(t[4])), ('title', t[1]), ('room', t[5])
            ]),
            talks
        )
    except:
        return 'ERROR'

def friends_events(login, password, eventname):
    try:
        for_participant(login, password)
        return map(
            lambda t: OrderedDict([
                ('login', login), ('eventname', eventname), 
                ('friendlogin', t[0])
            ]),
            sc.friends_on_event(cur, login, eventname)
        )
    except:
        return 'ERROR'
def recommended_talks(login, password, start_timestamp, end_timestamp, limit):
    return 'NOT IMPLEMENTED'











