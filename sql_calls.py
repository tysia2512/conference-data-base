import psycopg2
import string
import hashlib, uuid

def commit(cur):
    cur.execute('COMMIT;')

def find_participant(cur, login):
    cur.execute(
        "SELECT * FROM participant WHERE login = %s;",
        (login,)
    )
    return cur.fetchone()

def find_talk(cur, id):
    cur.execute(
        "SELECT * FROM talk WHERE id = %s;",
        (id,)
    )
    return cur.fetchone()

def all_talks(cur):
    cur.execute("SELECT * FROM talk")
    return cur.fetchall()

def add_participant(cur, login, hash, organizer):
    cur.execute(
        ("INSERT INTO participant (login, hash, organizer) "
        "VALUES (%s, %s, %s);"),
        (login, hash, organizer)
    )
    commit(cur)
        

def add_event(cur, name, start_time, end_time):
    cur.execute(
        ("INSERT INTO event (name, begin_time, end_time) "
        "VALUES (%s, %s, %s);"),
        (name, start_time, end_time)
    )
    commit(cur)

def add_rating(cur, talkid, participantlogin, rating):
    if not(int(rating) in range(0, 11)):
        raise 'rating not in range(0, 10)'
    cur.execute(
        ("INSERT INTO participant_rated_talk "
        + "(talkid, participantlogin, rating) "
        + "VALUES (%s, %s, %s)"
        + "ON CONFLICT (talkid, participantlogin) DO UPDATE SET "
        + "rating = %s;"),
        (talkid, participantlogin, rating, rating)
    )
    commit(cur)

def delete_proposal(cur, talk):
    cur.execute(
        "DELETE FROM talk_proposal WHERE id = %s;",
        (talk, )
        )
    commit(cur)

def add_talk(
    cur, 
    participantlogin, 
    speakerlogin, talkid, title, begin_time, room, initial_evaluation, eventname
    ):
    delete_proposal(cur, talkid)
    if eventname == '':
        cur.execute(
            ("INSERT INTO talk (id, title, eventname,"
            + " participantlogin, room, begin_time) "
            + "VALUES (%s, %s, NULL, %s, %s, %s);"),
            (talkid, title, speakerlogin, room, begin_time)   
        )
    else:
        cur.execute(
            ("INSERT INTO talk (id, title, eventname,"
            + " participantlogin, room, begin_time) "
            + "VALUES (%s, %s, %s, %s, %s, %s);"),
            (talkid, title, eventname, speakerlogin, room, begin_time)   
        )
    commit(cur)
    add_rating(cur, talkid, participantlogin, initial_evaluation)

def add_participant_registered_event(cur, participantlogin, eventname):
    cur.execute(
        ("INSERT INTO participant_registered_event "
        + "(participantlogin, eventname) "
        + "VALUES (%s, %s);"),
        (participantlogin, eventname)
    )
    commit(cur)

def add_participant_attends_talk(cur, participantlogin, talkid):
    cur.execute(
        ("INSERT INTO participant_attends_talk "
        + "(participantlogin, talkid) "
        + "VALUES (%s, %s);"),
        (participantlogin, talkid)
    )
    commit(cur)

def user_plan(cur, login):
    cur.execute(
        ("SELECT participant_registered_event.participantlogin, "
        + "talk.id, begin_time, title, room, talk.participantlogin FROM "
        + "participant_registered_event JOIN talk "
        + "ON participant_registered_event.eventname = talk.eventname "
        + "WHERE participant_registered_event.participantlogin = %s "
        + "AND talk.begin_time >= now()"
        + "ORDER BY begin_time;"),
        (login, )
    )
    return cur.fetchall()

def day_plan(cur, date):
    cur.execute(
        ("SELECT id, begin_time, title, room FROM "
        + "talk WHERE date(begin_time) = %s "
        + "ORDER BY room, begin_time;"),
        (date, )
    )
    return cur.fetchall()

def all_ratings(cur, talk):
    cur.execute(
        ("SELECT * FROM participant_rated_talk "
        + "WHERE talkid = %s;"),
        (talk, )
    )
    return cur.fetchall()

def attended_only_ratings(cur, talk):
    cur.execute(
        ("SELECT * FROM "
        + "participant_rated_talk JOIN participant_attends_talk "
        + "ON participant_rated_talk.participantlogin "
        + "= participant_attends_talk.participantlogin "
        + "AND participant_rated_talk.talkid "
        + "= participant_attends_talk.talkid "
        + "WHERE participant_attends_talk.talkid = %s;"),
        (talk, )
    )
    return cur.fetchall()

def talks_between(cur, start_time, end_time):
    cur.execute(
        ("SELECT * FROM talk "
        + "WHERE begin_time >= %s AND begin_time <= %s;"),
        (start_time, end_time)
    )
    return cur.fetchall()

def attendance_to_talk(cur, talk):
    cur.execute(
        ("SELECT * FROM talk JOIN participant_attends_talk "
        + "ON id = talkid WHERE id = %s"),
        (talk, )
    )
    return cur.fetchall()

def participant_attended_talks(cur, login):
    cur.execute(
        ("SELECT * FROM participant_attends_talk "
        + "JOIN talk ON talkid = id "
        + "WHERE participant_attends_talk.participantlogin = %s"),
        (login, )
    )
    return cur.fetchall()

def registered_to_event(cur, eventname):
    cur.execute(
        ("SELECT * FROM participant_registered_event "
        + "WHERE eventname = %s;"),
        (eventname, )
    )
    return cur.fetchall()

def delete_talk(cur, talk):
    cur.execute(
        "DELETE FROM talk WHERE id = %s RETURNING *;",
        (talk, )
    )
    return cur.fetchone()

def proposal(cur, talk, title, login, begin_time):
    cur.execute(
        (
            "INSERT INTO talk_proposal "
            + "(id, title, participantlogin, begin_time) "
            + "VALUES (%s, %s, %s, %s)"
        ),
        (talk, title, login, begin_time)
    )
    commit(cur)

def reject(cur, talk):
    cur.execute(
        "DELETE FROM talk_proposal WHERE id = %s RETURNING *;",
        (talk, )
    )
    rejected_proposal = cur.fetchone()
    cur.execute(
        (
            "INSERT INTO rejected_talk "
            + "(id, title, participantlogin, begin_time) "
            + "VALUES (%s, %s, %s, %s);"
        ),
        rejected_proposal
    )
    commit(cur)

def friend_request(cur, login1, login2):
    cur.execute(
        "SELECT invite(%s, %s);",
        (login1, login2)
    )
    (allowed,) = cur.fetchone()
    if not allowed:
        raise Exception('InputError: doubling invitation')

def all_rejected_talks(cur):
    cur.execute(
        "SELECT * FROM rejected_talk;"
    )
    return cur.fetchall()

def users_rejected_talks(cur, login):
    cur.execute(
        "SELECT * FROM rejected_talk WHERE participantlogin = %s;",
        (login,)
    )
    return cur.fetchall()

def proposals(cur):
    cur.execute(
        "SELECT * FROM talk_proposal;"
    )
    return cur.fetchall()

def friends_talks(cur, login, start_time, end_time):
    cur.execute(
        (
            "SELECT * FROM talk WHERE friends(participantlogin, %s) "
            + "AND begin_time >= %s AND begin_time <= %s"
            + "ORDER BY begin_time;"
        ),
        (login, start_time, end_time)
    )
    return cur.fetchall()

def friends_on_event(cur, login, eventname):
    cur.execute(
        (
            "SELECT participantlogin FROM participant_registered_event "
            + "WHERE friends(participantlogin, %s) AND eventname = %s;"
        ),
        (login, eventname)
    )
    return cur.fetchall()