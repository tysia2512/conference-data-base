
import psycopg2
import string
import hashlib, uuid
import sql_calls as sc
import bcrypt
import functions

# print functions.open1('conferencedb', '', '')
# print functions.organizer('d8578edf8458ce06fbc5bb76a58c5ca4', 'admin', 'pswd')
# print functions.event('admin', 'pswd', 'test_event', '2001-09-28','2001-09-30')
# print functions.event('admin', 'pswd', 'test_event1', '2015-10-02', '2015-10-03')
# print functions.user('admin', 'pswd', 'user1', 'pswd1')
# print functions.talk(
#     'admin', 'pswd', 'user1', 'talk1', 
#     'title1', '2001-09-28 10:00', 'room1', '0', 'test_event' 
#     )
# print functions.talk(
#     'admin', 'pswd', 'user1', 'talk2', 
#     'title1', '2001-09-29 11:00', 'room2', '0', 'test_event1' 
#     )
# print functions.register_user_for_event('user1', 'pswd1', 'test_event')
# print functions.register_user_for_event('admin', 'pswd', 'test_event')
# print "attendances:"
# print functions.attendance('user1', 'pswd1', 'talk1')
# print functions.attendance('admin', 'pswd', 'talk1')
# print 'next is unregistered attendance'
# print functions.attendance('user1', 'pswd1', 'talk2')
# print functions.attendance('user1', 'pswd1', 'talk2')
# print functions.evaluation('user1', 'pswd1', 'talk1', '9')
# print functions.user_plan('user1', 0)
# print functions.day_plan('2001-09-28')
# print functions.day_plan('2001-09-30')
# print functions.mean('talk1', '1')
# print functions.mean('talk1', '0')
# print functions.best_talks('2001-08-31', '2001-09-29 12:00', 3, '1')
# print functions.most_popular_talks('2001-08-31', '2001-09-29 12:00', 0)
# print functions.attended_talks('user1', 'pswd1')
# print functions.attended_talks('admin', 'pswd')
print functions.user('admin', 'pswd', 'user2', 'pswd2')
print functions.register_user_for_event('user2', 'pswd2', 'test_event1')
print functions.abandoned_talks('admin', 'pswd', 3)

